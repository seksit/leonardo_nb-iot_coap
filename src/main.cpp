#include <Arduino.h>
#include <ais-bc95.h>

ais_bc95 aisNB;

#define ATLSOFTSERIAL 1

#if ATLSOFTSERIAL 
	#include "AltSoftSerial.h"
#endif
AltSoftSerial altSerial;

String accesspointName = "NB-IoT with UDP and CoAP";

String serverIP = "100.106.77.133"; // Your Server IP
String serverPort = "54023"; // Your Server Port

String udpData = "HelloWorld";

const long interval = 20000;  //millisecond
unsigned long previousMillis = 0;

long cnt = 0;

void setup() {
    Serial.begin(9600);
    altSerial.begin(9600);
    while(!Serial);
    Serial.println("Starting...");

    aisNB.debug = true;
    aisNB.init(accesspointName, altSerial);
    aisNB.setupDevice(serverPort);

    String ip1 = aisNB.getDeviceIP();  
    pingRESP pingR = aisNB.pingIP(serverIP);
    previousMillis = millis();
}

void loop() {
    unsigned long currentMillis = millis();
    if (currentMillis - previousMillis >= interval) {
        cnt++;     
            
        // Send data in String 
        UDPSend udp = aisNB.sendUDPmsgStr(serverIP, serverPort, udpData+String(cnt));
    
        //Send data in HexString     
        //udpDataHEX = aisNB.str2HexStr(udpData);
        //UDPSend udp = aisNB.sendUDPmsg(serverIP, serverPort, udpDataHEX);
        previousMillis = currentMillis;
    }
    UDPReceive resp = aisNB.udpWaitResponse();
}